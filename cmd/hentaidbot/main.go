package main

import (
	"path/filepath"

	"github.com/kirillDanshin/dlog"
	"gitlab.com/hentaidb/robot/telegram/internal/bot"
	"gitlab.com/hentaidb/robot/telegram/internal/config"
	"gitlab.com/hentaidb/robot/telegram/internal/db"
	"gitlab.com/hentaidb/robot/telegram/internal/errors"
)

func init() {
	var err error
	config.Config, err = config.Open(filepath.Join(".", "configs", "development.yaml"))
	errors.Check(err)

	db.DB, err = db.Open(filepath.Join(".", "hentai.db"))
	errors.Check(err)

	bot.Bot, err = bot.New(config.Config.GetString("telegram.access_token"))
	errors.Check(err)
}

func main() {
	dlog.Ln("starting...")
	defer dlog.Ln("stopped!")
}
