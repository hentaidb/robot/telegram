package errors

func Check(err error) {
	if err == nil {
		return
	}

	panic(err.Error())
}
