package bot

import tg "github.com/toby3d/telegram"

type TelegramBot struct{ *tg.Bot }

var Bot = new(TelegramBot)

func New(accessToken string) (tb *TelegramBot, err error) {
	b, err := tg.New(accessToken)
	return &TelegramBot{Bot: b}, err
}
