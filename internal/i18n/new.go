package i18n

import "github.com/nicksnyder/go-i18n/v2/i18n"

func (bundle *Bundle) New(langs ...string) *i18n.Localizer {
	return i18n.NewLocalizer(bundle.Bundle, langs...)
}
